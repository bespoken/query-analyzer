# Overview for Batch Utterance Testing Recipe
This project illustrates using Bespoken to query voice assistants, without expecting a particular results.

Just add your queries to the file `queries.csv`. They will be sent to the voice assistant and the results captured. You can view the results in CSV file or DataDog.

## Common Setup
For test types, do the following:  
* Clone this repo
* Run `npm install` (and make sure Node.js is installed already - [go here if it is not](https://nodejs.org/en/download/))
* Create a Bespoken Virtual Device - [directions here](https://read.bespoken.io/end-to-end/setup)
* Add the Virtual Device to the json files for the tests under the `virtualDevices` section in [recordings-test.json](input/recordings-test.json)
* Make a copy of example.env and save it as `.env` - add the DATADOG_API_KEY here to publish results to DataDog

If you do not have a DataDog API Key, sign up for an account. You can then get the key from:  
`Integrations -> APIs -> API Keys`

## Utterance Tests
Fill out the queries.csv file with utterances that you want to test.

To run the tests locally from the command-line, enter:
```
npm run analyze
```

# Running in Gitlab CI  
## Setup
Add the environment variables need for execution to `Settings -> CI / CD -> Variables`

The variables include:

| Variable | Description
| --- | --- |
 DATADOG_API_KEY | The DataDog API key. Needed to publish results to DataDog.

## Adhoc Runs
To run the tests inside Gitlab, do the following:  
* Go to `CI / CD -> Pipelines`
* Select `Run Pipeline`
* Click `Run Pipeline`

That's it! The appropriate job will then run.

## Scheduled Runs
Go to `CI / CD -> Schedules` to set a routine schedule for running the tests.

Be sure to set the JOB_TYPE variable with the appropriate value.

# Viewing Results
## CSV Results
The CSV results are captured with each run.

You can view them in Gitlab by going to:  
`CI / CD -> Jobs`

Then click on the job that corresponds to your run. The test results are under:  
`Job Artifacts -> Browse`

## DataDog Results
DataDog results are published for each test.

Screenshot of the results, for illustration:  

[<img src="docs/DataDogDashboard.png" width="50%">](docs/DataDogDashboard.png)

To create the same Dashboard, do the following:
* Log into DataDog
* Select "Dashboard" -> "New Dashboard".
* Click on the gear icon on the top right
* Select "Import dashboard JSON"
* Choose the file: `input/DataDog-QueryAnalyzerDashboard.json`

You should be all set!

More info on configuring DataDog dashboards and monitoring is available here:  
https://gitlab.com/bespoken/batch-tester/-/blob/master/docs/datadog.md

